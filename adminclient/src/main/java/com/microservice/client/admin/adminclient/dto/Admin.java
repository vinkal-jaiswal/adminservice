package com.microservice.client.admin.adminclient.dto;

import java.io.Serializable;

public class Admin implements Serializable {

	private String name;

	private String dept;

	private String address;


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDept() {
		return dept;
	}

	public void setDept(String dept) {
		this.dept = dept;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
}
