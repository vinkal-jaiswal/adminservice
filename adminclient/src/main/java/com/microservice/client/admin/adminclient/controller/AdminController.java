package com.microservice.client.admin.adminclient.controller;


import com.microservice.client.admin.adminclient.dto.Patient;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
@RequestMapping("/")
public class AdminController {


	@Autowired
	RestTemplate restTemplate;


	@GetMapping("/")
	public String getHome(){
		return  "admin service from port 8083";
	}

	//test call
	//@HystrixCommand(fallbackMethod = "fallback")
	@GetMapping("/get-admin-patients")
	public String getPatients() {
		List<Patient> patientList = restTemplate.getForObject("http://patients-service/get-patients-list", List.class);
		return patientList.toString();
	}

	// a fallback method to be called if failure happened
	public Patient fallback(Throwable hystrixCommand) {
		return new Patient();

	}

}
